package com.ranull.afksit.manager;

import com.Zrips.CMI.CMI;
import com.ranull.afksit.AFKSit;
import com.ranull.sittable.Sittable;
import de.sprax2013.betterchairs.ChairManager;
import dev.geco.gsit.api.GSitAPI;
import dev.geco.gsit.objects.GetUpReason;
import me.DemoPulse.UltimateChairs.API.UltimateChairsAPI;
import net.achymake.chairs.settings.ChairSettings;
import net.apcat.simplesit.SimpleSitPlayer;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class SitManager {
    private final AFKSit plugin;
    private final List<UUID> uuidList;
    private SitPlugin sitPlugin;

    public SitManager(AFKSit plugin) {
        this.plugin = plugin;
        this.uuidList = new ArrayList<>();

        findSitPlugin();
    }

    public void findSitPlugin() {
        sitPlugin = setSitPlugin();
    }

    public void standAllPlayers() {
        for (Player player : plugin.getServer().getOnlinePlayers()) {
            if (plugin.getSitManager().isSitting(player) && plugin.getSitManager().isAFKSitting(player)) {
                plugin.getSitManager().stand(player);
                plugin.getSitManager().removeAFKSitting(player);
            }
        }
    }

    public boolean shouldSit(Player player) {
        Block eyeBlock = player.getEyeLocation().getBlock();

        return !player.getGameMode().name().equals("SPECTATOR")
                && player.getLocation().getBlock().getRelative(BlockFace.DOWN).getType().isSolid()
                && (!eyeBlock.getType().name().equals("WATER") && !eyeBlock.getType().name().equals("STATIONARY_WATER")
                && !eyeBlock.getType().name().equals("LAVA") && !eyeBlock.getType().name().equals("STATIONARY_LAVA"));
    }

    public boolean isAFKSitting(Player player) {
        return uuidList.contains(player.getUniqueId());
    }

    public void addAFKSitting(Player player) {
        uuidList.add(player.getUniqueId());
    }

    public void removeAFKSitting(Player player) {
        uuidList.remove(player.getUniqueId());
    }

    @SuppressWarnings("UnusedReturnValue")
    public boolean sit(Player player) {
        Location location = player.getLocation().clone();

        location.setY(Math.ceil(player.getLocation().getY()));

        Block block = location.getBlock().getRelative(BlockFace.DOWN);

        try {
            switch (sitPlugin) {
                case SITTABLE:
                    return Sittable.sitOnGround(player);
                case GSIT:
                    return GSitAPI.createSeat(block, player) != null;
                case SIMPLESIT:
                    SimpleSitPlayer simpleSitPlayer = new SimpleSitPlayer(player);

                    simpleSitPlayer.setSitting(true);

                    return simpleSitPlayer.isSitting();
                case BETTERCHAIRS:
                    return ChairManager.getInstance() != null && ChairManager.getInstance().create(player, block);
                case ULTIMATECHAIRS:
                    new me.DemoPulse.UltimateChairs.Chair(player, player.getLocation().getBlock(), true);

                    return true;
                case CHAIRS:
                    ChairSettings.sitOnCommand(player, player.getLocation());

                    return true;
                case CMI:
                    CMI.getInstance().getAnimationManager().sit(player);

                    return true;
            }
        } catch (Exception | Error ignored) {
        }

        return false;
    }

    @SuppressWarnings("UnusedReturnValue")
    public boolean stand(Player player) {
        try {
            switch (sitPlugin) {
                case SITTABLE:
                    return player.leaveVehicle();
                case GSIT:
                    return GSitAPI.removeSeat(player, GetUpReason.PLUGIN);
                case SIMPLESIT:
                    SimpleSitPlayer simpleSitPlayer = new SimpleSitPlayer(player);

                    simpleSitPlayer.setSitting(false);

                    return !simpleSitPlayer.isSitting();
                case BETTERCHAIRS:
                    if (ChairManager.getInstance() != null && player.getVehicle() != null) {
                        de.sprax2013.betterchairs.Chair chair = ChairManager.getInstance().getChair(player.getVehicle());

                        if (chair != null) {
                            ChairManager.getInstance().destroy(chair, true);

                            return true;
                        }
                    }

                    return false;
                case ULTIMATECHAIRS:
                    me.DemoPulse.UltimateChairs.Chair chair = UltimateChairsAPI.getChair(player);

                    if (chair != null) {
                        chair.dismount();

                        return true;
                    }

                    return false;
                case CHAIRS:
                    if (player.getVehicle() != null) {
                        ChairSettings.stand(player, player.getVehicle());

                        return true;
                    }

                    return false;
                case CMI:
                    CMI.getInstance().getAnimationManager().removePlayerFromChair(player);

                    return true;
            }
        } catch (Exception | Error ignored) {
        }

        return false;
    }

    public boolean isSitting(Player player) {
        try {
            switch (sitPlugin) {
                case SITTABLE:
                    return Sittable.isEntitySitting(player);
                case GSIT:
                    return GSitAPI.isSitting(player);
                case SIMPLESIT:
                    return new SimpleSitPlayer(player).isSitting();
                case BETTERCHAIRS:
                    return ChairManager.getInstance() != null && player.getVehicle() != null
                            && ChairManager.getInstance().getChair(player.getVehicle()) != null;
                case ULTIMATECHAIRS:
                    return UltimateChairsAPI.getChair(player) != null;
                case CHAIRS:
                    return ChairSettings.isSitting(player);
                case CMI:
                    return CMI.getInstance().getAnimationManager().isSitting(player);
            }
        } catch (Exception | Error ignored) {
        }

        return false;
    }

    private SitPlugin setSitPlugin() {
        Plugin sittablePlugin = plugin.getServer().getPluginManager().getPlugin("Sittable");

        if (sittablePlugin != null && sittablePlugin.isEnabled()) {
            return SitPlugin.SITTABLE;
        }

        Plugin gSitPlugin = plugin.getServer().getPluginManager().getPlugin("GSit");

        if (gSitPlugin != null && gSitPlugin.isEnabled()) {
            return SitPlugin.GSIT;
        }

        Plugin simpleSitPlugin = plugin.getServer().getPluginManager().getPlugin("SimpleSit");

        if (simpleSitPlugin != null && simpleSitPlugin.isEnabled()) {
            return SitPlugin.SIMPLESIT;
        }

        Plugin BetterChairsPlugin = plugin.getServer().getPluginManager().getPlugin("BetterChairs");

        if (BetterChairsPlugin != null && BetterChairsPlugin.isEnabled()) {
            return SitPlugin.BETTERCHAIRS;
        }

        Plugin ultimateChairsPlugin = plugin.getServer().getPluginManager().getPlugin("UltimateChairs");

        if (ultimateChairsPlugin != null && ultimateChairsPlugin.isEnabled()) {
            return SitPlugin.ULTIMATECHAIRS;
        }

        Plugin chairsPlugin = plugin.getServer().getPluginManager().getPlugin("Chairs");

        if (chairsPlugin != null && chairsPlugin.isEnabled()) {
            return SitPlugin.CHAIRS;
        }

        Plugin cmiPlugin = plugin.getServer().getPluginManager().getPlugin("CMI");

        if (cmiPlugin != null && cmiPlugin.isEnabled()) {
            return SitPlugin.CMI;
        }

        return SitPlugin.NONE;
    }

    public enum SitPlugin {
        SITTABLE,
        GSIT,
        SIMPLESIT,
        BETTERCHAIRS,
        ULTIMATECHAIRS,
        CHAIRS,
        CMI,
        NONE
    }
}
