package com.ranull.afksit;

import com.ranull.afksit.command.AFKSitCommand;
import com.ranull.afksit.listener.PlayerQuitListener;
import com.ranull.afksit.listener.afkplus.AFKStartListener;
import com.ranull.afksit.listener.afkplus.AFKStopListener;
import com.ranull.afksit.listener.cmi.CMIAfkEnterListener;
import com.ranull.afksit.listener.cmi.CMIAfkLeaveListener;
import com.ranull.afksit.listener.essentials.AfkStatusChangeListener;
import com.ranull.afksit.manager.SitManager;
import org.bstats.bukkit.MetricsLite;
import org.bukkit.command.PluginCommand;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public final class AFKSit extends JavaPlugin {
    private SitManager sitManager;

    @Override
    public void onEnable() {
        sitManager = new SitManager(this);

        registerMetrics();
        registerCommands();
        registerListeners();
    }

    @Override
    public void onDisable() {
        sitManager.standAllPlayers();
        unregisterListeners();
    }

    public void reload() {
        sitManager.standAllPlayers();
        sitManager.findSitPlugin();
        unregisterListeners();
        registerListeners();
    }

    private void registerMetrics() {
        new MetricsLite(this, 17339);
    }

    private void registerCommands() {
        PluginCommand afkSitPluginCommand = getCommand("afksit");

        if (afkSitPluginCommand != null) {
            AFKSitCommand afkSitCommand = new AFKSitCommand(this);

            afkSitPluginCommand.setExecutor(afkSitCommand);
            afkSitPluginCommand.setTabCompleter(afkSitCommand);
        }
    }

    public void registerListeners() {
        getServer().getPluginManager().registerEvents(new PlayerQuitListener(this), this);

        Plugin essentialsPlugin = getServer().getPluginManager().getPlugin("Essentials");

        if (essentialsPlugin != null && essentialsPlugin.isEnabled()) {
            getServer().getPluginManager().registerEvents(new AfkStatusChangeListener(this), this);
        }

        Plugin cmiPlugin = getServer().getPluginManager().getPlugin("CMI");

        if (cmiPlugin != null && cmiPlugin.isEnabled()) {
            getServer().getPluginManager().registerEvents(new CMIAfkEnterListener(this), this);
            getServer().getPluginManager().registerEvents(new CMIAfkLeaveListener(this), this);
        }

        Plugin afkPlusPlugin = getServer().getPluginManager().getPlugin("AFKPlus");

        if (afkPlusPlugin != null && afkPlusPlugin.isEnabled()) {
            getServer().getPluginManager().registerEvents(new AFKStartListener(this), this);
            getServer().getPluginManager().registerEvents(new AFKStopListener(this), this);
        }
    }

    public void unregisterListeners() {
        HandlerList.unregisterAll(this);
    }

    public SitManager getSitManager() {
        return sitManager;
    }
}
