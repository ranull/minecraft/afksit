package com.ranull.afksit.command;

import com.ranull.afksit.AFKSit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AFKSitCommand implements CommandExecutor, TabExecutor {
    private final AFKSit plugin;

    public AFKSitCommand(AFKSit plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender commandSender, @NotNull Command command,
                             @NotNull String string, String[] args) {
        if (args.length == 0) {
            commandSender.sendMessage(ChatColor.GREEN + "AFKSit " + ChatColor.DARK_GRAY
                    + ChatColor.RESET + ChatColor.DARK_GRAY + "v" + plugin.getDescription().getVersion());

            commandSender.sendMessage(ChatColor.GREEN + "/afksit " + ChatColor.DARK_GRAY + "-"
                    + ChatColor.RESET + " Plugin info");

            if (commandSender.hasPermission("afksit.reload")) {
                commandSender.sendMessage(ChatColor.GREEN + "/afksit reload " + ChatColor.DARK_GRAY + "-"
                        + ChatColor.RESET + " Reload plugin");
            }

            commandSender.sendMessage(ChatColor.DARK_GRAY + "Author: " + ChatColor.GREEN + "Ranull");
        } else if (args[0].equals("reload")) {
            if (commandSender.hasPermission("afksit.reload")) {
                plugin.reload();
                commandSender.sendMessage(ChatColor.GREEN + "AFKSit" + ChatColor.DARK_GRAY + " » "
                        + ChatColor.RESET + "Reloaded plugin.");
            } else {
                commandSender.sendMessage(ChatColor.GREEN + "AFKSit" + ChatColor.DARK_GRAY + " » "
                        + ChatColor.RESET + "No permission.");
            }
        }

        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender commandSender, @NotNull Command command,
                                      @NotNull String string, @NotNull String[] args) {
        if (commandSender.hasPermission("afksit.reload")) {
            return Collections.singletonList("reload");
        }

        return new ArrayList<>();
    }
}
