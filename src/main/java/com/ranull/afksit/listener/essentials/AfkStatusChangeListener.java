package com.ranull.afksit.listener.essentials;

import com.ranull.afksit.AFKSit;
import net.ess3.api.events.AfkStatusChangeEvent;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

public class AfkStatusChangeListener implements Listener {
    private final AFKSit plugin;

    public AfkStatusChangeListener(AFKSit plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onAfkStatusChange(AfkStatusChangeEvent event) {
        Player player = event.getAffected().getBase();

        if (!event.getAffected().isAfk()) {
            if (!plugin.getSitManager().isSitting(player) && plugin.getSitManager().shouldSit(player)) {
                plugin.getSitManager().addAFKSitting(player);
                plugin.getSitManager().sit(player);
            }
        } else {
            if (plugin.getSitManager().isAFKSitting(player)) {
                if (plugin.getSitManager().isSitting(player)) {
                    plugin.getSitManager().stand(player);
                }

                plugin.getSitManager().removeAFKSitting(player);
            }
        }
    }
}
