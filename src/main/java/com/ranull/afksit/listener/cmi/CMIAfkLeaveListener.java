package com.ranull.afksit.listener.cmi;

import com.Zrips.CMI.events.CMIAfkLeaveEvent;
import com.ranull.afksit.AFKSit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

public class CMIAfkLeaveListener implements Listener {
    private final AFKSit plugin;

    public CMIAfkLeaveListener(AFKSit plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onCMIAfkEnter(CMIAfkLeaveEvent event) {
        Player player = event.getPlayer();

        if (plugin.getSitManager().isAFKSitting(player)) {
            if (plugin.getSitManager().isSitting(player)) {
                plugin.getSitManager().stand(player);
            }

            plugin.getSitManager().removeAFKSitting(player);
        }
    }
}
