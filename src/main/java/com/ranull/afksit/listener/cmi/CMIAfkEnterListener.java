package com.ranull.afksit.listener.cmi;

import com.Zrips.CMI.events.CMIAfkEnterEvent;
import com.ranull.afksit.AFKSit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

public class CMIAfkEnterListener implements Listener {
    private final AFKSit plugin;

    public CMIAfkEnterListener(AFKSit plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onCMIAfkEnter(CMIAfkEnterEvent event) {
        Player player = event.getPlayer();

        if (!plugin.getSitManager().isSitting(player) && plugin.getSitManager().shouldSit(player)) {
            plugin.getSitManager().addAFKSitting(player);
            plugin.getSitManager().sit(player);
        }
    }
}
