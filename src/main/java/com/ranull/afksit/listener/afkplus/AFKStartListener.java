package com.ranull.afksit.listener.afkplus;

import com.ranull.afksit.AFKSit;
import net.lapismc.afkplus.api.AFKStartEvent;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

public class AFKStartListener implements Listener {
    private final AFKSit plugin;

    public AFKStartListener(AFKSit plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onAFKStart(AFKStartEvent event) {
        Player player = plugin.getServer().getPlayer(event.getPlayer().getUUID());

        if (player != null && !plugin.getSitManager().isSitting(player)
                && plugin.getSitManager().shouldSit(player)) {
            plugin.getSitManager().addAFKSitting(player);
            plugin.getSitManager().sit(player);
        }
    }
}
