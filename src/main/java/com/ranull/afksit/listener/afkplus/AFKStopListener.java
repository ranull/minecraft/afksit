package com.ranull.afksit.listener.afkplus;

import com.ranull.afksit.AFKSit;
import net.lapismc.afkplus.api.AFKStopEvent;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

public class AFKStopListener implements Listener {
    private final AFKSit plugin;

    public AFKStopListener(AFKSit plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onAFKStop(AFKStopEvent event) {
        Player player = plugin.getServer().getPlayer(event.getPlayer().getUUID());

        if (player != null && plugin.getSitManager().isAFKSitting(player)) {
            if (plugin.getSitManager().isSitting(player)) {
                plugin.getSitManager().stand(player);
            }

            plugin.getSitManager().removeAFKSitting(player);
        }
    }
}
