package com.ranull.afksit.listener;

import com.ranull.afksit.AFKSit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerQuitListener implements Listener {
    private final AFKSit plugin;

    public PlayerQuitListener(AFKSit plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onPlayerQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();

        if (plugin.getSitManager().isAFKSitting(player)) {
            plugin.getSitManager().removeAFKSitting(player);
        }
    }
}
